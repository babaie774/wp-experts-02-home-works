<?php
/*
 استفاده از علامت های براکت در جلوی اسم آرایه باعث میشود مقداری که در سمت راست قرار دارد به آرایه اضافه شود
یعنی در خط 13 این صفحه وقتی متغییر $number با عدد 3 جمع می شود مقدار نهایی آن به عنوان یک درایه در آرایه $newNumbers قرار میگیرد

 */

function makeArray()
{
    $numbers    = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    $newNumbers = array();
    foreach ($numbers as $number) {
        $newNumbers[] = $number + 3;

    }

    return $newNumbers;
}