<?php
include "functions.php";
$posts = getPostsByDates("2017-08-10", "2017-10-06");
?>
<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Posts</title>
</head>
<body>
<table>
    <tr>
        <th>شناسه مطلب</th>
        <th>عنوان مطلب</th>
        <th>تاریخ ایجاد</th>
    </tr>
    <?php while ($post = $posts->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><?php echo $post['post_id']; ?></td>
            <td><?php echo $post['post_title']; ?></td>
            <td><?php echo $post['post_created_at']; ?></td>
        </tr>
    <?php endwhile; ?>
</table>
</body>
</html>