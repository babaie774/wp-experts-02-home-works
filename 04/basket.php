<?php
require "functions.php";
$basketItems = getBasket();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>لیست محصولات</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style>
        body {
            padding: 50px 0;
            direction: rtl;
        }
    </style>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">نمایش سبد خرید</div>
                <div class="panel-body">
                    <div class="row">
                        <form action="update_cart_item.php" method="post">
                            <p><?php echo intval($_SESSION['basket']['total']); ?></p>
                            <table class="table table-bordered table-hover table-stripe">
                                <tr>
                                    <th>نام محصول</th>
                                    <th>قیمت فی</th>
                                    <th>تعداد</th>
                                    <th>قیمت کل</th>
                                    <th>عملیات</th>
                                </tr>
								<?php foreach ( $basketItems as $product_id => $item ) : ?>
                                    <tr>
                                        <td><?php echo $item['title']; ?></td>
                                        <td><?php echo $item['price'] ?></td>
                                        <td>
                                            <input type="number" name="item_count[<?php echo $product_id; ?>]"
                                                   value="<?php echo $item['count']; ?>">
                                            <button type="submit" value="<?php echo $product_id; ?>"
                                                    name="update_count">Update</button>
                                            <button type="submit" value="<?php echo $product_id; ?>"
                                                    name="remove_item">Remove</button>
                                        </td>
                                        <td><?php echo $item['price'] * $item['count']; ?></td>
                                        <td>
                                        </td>
                                    </tr>
								<?php endforeach; ?>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>

