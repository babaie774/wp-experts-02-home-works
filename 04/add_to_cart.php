<?php
include "functions.php";

if ( isset( $_GET['product_id'] ) && intval( $_GET['product_id'] ) > 0 ) {

	$product_id = $_GET['product_id'];
	$product    = findProduct( $product_id );
	if ( ! isset( $_SESSION['basket']['total'] ) ) {
		$_SESSION['basket']['total'] = 0;
	}
	if ( isset( $_SESSION['basket']['items'][ $product_id ] ) ) {
		$_SESSION['basket']['items'][ $product_id ]['count'] ++;
	} else {
		$_SESSION['basket']['items'][ $product_id ] = [
			'price' => $product['product_price'],
			'title' => $product['product_title'],
			'count' => 1
		];
	}
	$_SESSION['basket']['total'] += $product['product_price'];
}
print_r( $_SESSION );
