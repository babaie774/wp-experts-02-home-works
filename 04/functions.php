<?php
include "init.php";

function getProducts(){
	global $db;

	$stmt = $db->prepare("
		SELECT * FROM products
	");
	$stmt->execute();
	return $stmt;
}
function findProduct($product_id)
{
	global $db;
	$stmt = $db->prepare("
							SELECT * 
							FROM products 
							WHERE product_id={$product_id}
							LIMIT 1"
	);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_ASSOC);
}

function getBasket(){
	return isset($_SESSION['basket']['items']) ? $_SESSION['basket']['items'] : [];
}
