<?php
include "functions.php";
if ( isset( $_POST['update_count'] ) ) {
	$product_id = intval( $_POST['update_count'] );
	$count      = intval( $_POST['item_count'][ $product_id ] );
	if ( isset( $_SESSION['basket']['items'][ $product_id ] ) ) {
		$count                                               = $count <= 0 ? 0 : $count;
		$old_count                                           = $_SESSION['basket']['items'][ $product_id ]['count'];
		$diff_count                                          = $count - $old_count;
		$diff_price                                          = $diff_count * $_SESSION['basket']['items'][ $product_id ]['price'];
		$_SESSION['basket']['total']                         += $diff_price;
		$_SESSION['basket']['items'][ $product_id ]['count'] = $count;
		if ( intval( $count ) <= 0 ) {
			unset( $_SESSION['basket']['items'][ $product_id ] );
		}

	}

	header( "Location: basket.php" );

}
if ( isset( $_POST['remove_item'] ) ) {
	$product_id = intval( $_POST['remove_item'] );

	if ( isset( $_SESSION['basket']['items'][ $product_id ] ) ) {
		$count                       = $_SESSION['basket']['items'][ $product_id ]['count'];
		$_SESSION['basket']['total'] -= ( $count * $_SESSION['basket']['items'][ $product_id ]['price'] );
		unset( $_SESSION['basket']['items'][ $product_id ] );
	}
	header( "Location: basket.php" );
}