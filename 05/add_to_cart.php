<?php
include "class-basket.php";

if ( isset( $_GET['product_id'] ) && intval( $_GET['product_id'] ) > 0 ) {
	$basket = new Basket();
	$basket->add( intval( $_GET['product_id'] ) );
}