<?php
require "db.php";
function insertData($fullName,$mobile,$content){
    global $db;
    $created_at = date("Y-m-d H:i:s");
    $code = md5($fullName.$mobile.$content);
    $code = substr($code,0,10);
    $code = strtoupper($code);
    $stmt = $db->prepare("INSERT INTO complaints(`full_name`,`mobile`,`content`,`created_at`,`code`)
									   VALUES('{$fullName}','{$mobile}','$content','{$created_at}','{$code}')");
    $stmt->execute();
    return $code;
}
function getData($date=null){
    global $db;
    $sql = "SELECT * FROM complaints";
    if(!empty($date))
    {
        $sql.=" WHERE DATE(created_at)=DATE({$date})";

    }
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt;

}
function setReply($id,$replyContent){

    global $db;
    $stmt = $db->prepare("UPDATE complaints SET reply='{$replyContent}' WHERE id={$id} LIMIT 1");
    return $stmt->execute();

}
function findReply($code){
    global $db,$table;
    $code = strtoupper($code);
    $stmt = $db->prepare("SELECT reply FROM {$table} WHERE code='{$code}' LIMIT 1");
    $stmt->execute();
    return $stmt;
}
function getItem($id){
    global $db;
    $stmt = $db->prepare("SELECT * FROM complaints WHERE id={$id}");
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

